#include <iostream>
#include <string>


using namespace std;
void display(int arr[],int size)
{
	for (int i=0;i< size;i++){
		std::cout<<arr[i]<<" ";
	}
	std::cout<<std::endl;
}
void swap(int *a, int *b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}
void selectionsort(int arr[],int size)
{
	int i, j, min;
	for (int i=0; i<size-1; i++)
	{
		min = i;
		for(j= i+1;j<size ; j++)
		{
			if (arr[j]<arr[min])
				min = j;
		}
		swap(&arr[min],&arr[i]);
	}

}

int main()
{
        std::cout<<"\nSelection Sort...";
	int * arr;
	int size = 10;
	arr = new (nothrow) int[size+1];
	if (arr == nullptr){
		std::cout<<"ERROR: memory allocation failed!!!"<<endl;
		exit(1);
	}
	std::cout<<"\nInserting 10 random numbers between 1 to 40 in an array...";
	for (int i=0;i<size;i++)
	{
		arr[i] = rand() % 40 +1;
	}
        std::cout<<"\nDispalying unsorted array...";
	display(arr,size);
	selectionsort(arr,size);
	std::cout<<"\nDispalying sorted array...";
	display(arr,size);
	delete[] arr;
return 0;
}

