#include <iostream>
#include <string>


using namespace std;
void display(int arr[],int size)
{
	for (int i=0;i< size;i++){
		std::cout<<arr[i]<<" ";
	}
	std::cout<<std::endl;
}
void insertionsort(int arr[],int size)
{
	int i, j, key;
	for (int i=1; i<size; i++) // i=1 leaving first index(0) as a key element
	{
		key = arr[i];
		j = i-1;
		while(j>= 0 && arr[j] > key)
		{
			arr[j+1] = arr[j];
			j = j - 1;
		}
		arr[j+1] = key;
	}

}

int main()
{
        std::cout<<"Insertion Sort...";
	int * arr;
	int size = 10;
	arr = new (nothrow) int[size+1];
	if (arr == nullptr){
		std::cout<<"ERROR: memory allocation failed!!!"<<endl;
		exit(1);
	}
	std::cout<<"\nInserting 10 random numbers between 1 to 40 in an array...";
	for (int i=0;i<size;i++)
	{
		arr[i] = rand() % 40 +1;
	}
        std::cout<<"\nDispalying unsorted array...";
	display(arr,size);
	insertionsort(arr,size);
	std::cout<<"Dispalying sorted array...";
	display(arr,size);
	delete[] arr;
return 0;
}

